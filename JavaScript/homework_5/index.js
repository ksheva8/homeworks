function createNewUser() {
  let name = prompt("Enter your name", "");
  let surname = prompt("Enter your surname", "");
  let birthday = prompt("Введите дату рождения в формате дд.мм.гггг", "");

  let newUser = {
    firstName: name,
    lastName: surname,
    age: birthday,
    getLogin: function () {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge() {
      let userBirthDay = birthday.slice(0, 2);
      let userBirthMonth = birthday.slice(3, 5);
      let userBirthYear = birthday.slice(6);
      let currentData = new Date();
      let currentDay = currentData.getDate();
      let currentMonth = currentData.getMonth() + 1;
      let currentYear = currentData.getFullYear();
      if (currentMonth > userBirthMonth) {
        return currentYear - userBirthYear;
      } else if (currentMonth <= userBirthMonth) {
        if (currentDay >= userBirthDay) {
          return currentYear - userBirthYear;
        } else {
          return currentYear - userBirthYear - 1;
        }
      }
    },
    getPassword() {
      console.log(name);
      // return (
      //   firstName[0].toUpperCase() +
      //   lastName.toLowerCase() +
      //   birthday.slice(6, 10)
      // );
    },
  };
  return newUser;
}

let newUserObj = createNewUser();
console.log(
  `${newUserObj.getLogin()} ${newUserObj.getAge()} ${newUserObj.getPassword()}`
);
