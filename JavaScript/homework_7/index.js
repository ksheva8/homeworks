function createList(arr, parent = document.body) {
  let ul = document.createElement("ul");
  arr.map((item) => {
    ul.insertAdjacentHTML("beforeend", `<li>${item}</li>`);
  });
  parent.append(ul);
}
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
