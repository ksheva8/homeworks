function createNewUser() {
  let name = prompt("Enter your name", "");
  let surname = prompt("Enter your surname", "");
  let newUser = {
    firstName: name,
    lastName: surname,
    getLogin: function () {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
  };
  return newUser;
}

let newUserObj = createNewUser();
console.log(newUserObj.getLogin());
