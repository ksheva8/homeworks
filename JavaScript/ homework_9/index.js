let tabsBtn = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content");

tabsBtn.forEach(function (item) {
  item.addEventListener("click", function () {
    let currentBtn = item;
    let idBtn = currentBtn.getAttribute("data-number");
    let currentTab = document.querySelector(idBtn);

    if (!currentBtn.classList.contains("active")) {
      tabsBtn.forEach(function (item) {
        item.classList.remove("active");
      });

      tabsContent.forEach(function (item) {
        item.classList.remove("active");
      });

      currentBtn.classList.add("active");
      currentTab.classList.add("active");
    }
  });
});
