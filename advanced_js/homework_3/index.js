class Emploee {
  constructor(generalInfo) {
    this._name = generalInfo.name;
    this._age = generalInfo.age;
    this._salary = generalInfo.salary;
  }
  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
  }
  get age() {
    return this._age;
  }
  set age(age) {
    this._age = age;
  }
  get salary() {
    return this._salary + "$";
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Emploee {
  constructor(generalInfo, lang) {
    super(generalInfo);
    this._lang = generalInfo.lang;
    this._salary = generalInfo.salary;
  }
  get lang() {
    return this._lang;
  }

  set lang(lang) {
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3 + "$";
  }
  set salary(salary) {
    this._salary = salary;
  }
}

let emploee = new Emploee({
  name: "Ilon",
  age: 49,
  salary: 100000000,
});

let programmer = new Programmer({
  name: "Vasya",
  age: 21,
  salary: 1000,
  lang: `"English", "German", "Portugal"`,
});
