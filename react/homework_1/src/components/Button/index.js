import PropTypes from "prop-types";

const Button = ({ bgc = "black", text = "Button",className, onClick = () => {} }) => {
  return (
    <button 
    className={`${className}`}
      onClick={() => {
        onClick();
      }}
      style={{
        backgroundColor: bgc,
      }}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  bgc: PropTypes.string,
};
export default Button;
