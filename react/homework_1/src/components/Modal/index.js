import React from "react";
import PropTypes from "prop-types";
class Modal extends React.PureComponent {
  render() {
    const {
      header,
      bgc,
      closeButton,
      text,
      actions,
      closeModalHandler,
      children,
      className,
    } = this.props;

    return (
      <div
        className="modal-container"
        
        onClick={(e) => {
          e.target === e.currentTarget && closeModalHandler();
        }}
      >
        <div className="modal"
        style={{
          backgroundColor: bgc,
        }}>
         <div className={className}>
         {closeButton && (
            <span className="close"
              onClick={() => {
                closeModalHandler();
              }}
            >
              X
            </span>
          )}
          <h4 className="modal__header">{header}</h4>
         </div>
          <p className="modal__text">{text}</p>
          <div className="auxiliaries">
          {actions}
          {children}
          </div>
        
        </div>
      </div>
    );
  }
}

Modal.defaultProps = {
  header: "No header",
  closeButton: false,
  text: "No text",
  actions: "No actions",
  closeModalHandler: () => {},
};

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  children: PropTypes.elementType,
};
export default Modal;
