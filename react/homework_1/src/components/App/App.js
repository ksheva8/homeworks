import Button from "../Button";
import Modal from "../Modal";
import React from "react";

class App extends React.PureComponent {
  state = {
    firstModal: false,
    secondModal: false,
  };

  componentDidMount() {
    fetch("./items.json")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState((state) => {
          console.log(data);
          return {
            ...state,
            items: data,
          };
        });
      });
  }

  firstModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        firstModal: !state.firstModal,
      };
    });
  };

  secondModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        secondModal: !state.secondModal,
      };
    });
  };


  render() {
    return (
      <>
        <div className="App">
          <Button className="first_modal_button"
            text="Open first modal"
            bgc=""
            onClick={this.firstModalHandler}
          />
          <Button text="Open second modal" bgc="" className="second_modal_button"
          onClick={this.secondModalHandler} />
          {this.state.firstModal && (
            <Modal
              className="red_modal_nav"
              text="Once you delete this file , it won't be possible to undo this action . Are you sure you want to delete it ?"
              header="Do you want to delete this file ?"
              closeButton={true}
              actions={
                <>
                <button className="auxiliary_btn">Ok</button>
                <button className="auxiliary_btn" >Cancel</button>
                </>
              }
              bgc="#eb4034"
              closeModalHandler={this.firstModalHandler}
            />
          )}
         

         {this.state.secondModal && (
            <Modal
              className="green_modal_nav"
              text="If you want to add this file , please click on --> Ok. You can choose more files !"
              header="Do you want to add this file ?"
              closeButton={true}
              actions={
                <>
                <button className="auxiliary_btn__grn">Ok</button>
                <button className="auxiliary_btn__grn" >Cancel</button>
                </>
              }
              bgc="#52ba68"
              closeModalHandler={this.secondModalHandler}
            />
          )}
        </div>
      </>
    );
  }
}

export default App;
