let tabsBtn = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content");

tabsBtn.forEach(function (item) {
  item.addEventListener("click", function () {
    let currentBtn = item;
    let idBtn = currentBtn.getAttribute("data-number");
    let currentTab = document.querySelector(idBtn);

    if (!currentBtn.classList.contains("active")) {
      tabsBtn.forEach(function (item) {
        item.classList.remove("active");
      });

      tabsContent.forEach(function (item) {
        item.classList.remove("active");
      });

      currentBtn.classList.add("active");
      currentTab.classList.add("active");
    }
  });
});

let imgTabsTitles = document.querySelectorAll(".amazing-work-tabs-title");
let imgTabsContent = document.querySelectorAll(".image-container");
let imgTabsName;

for (let i = 0; i < imgTabsTitles.length; i++) {
  imgTabsTitles[i].addEventListener("click", selectImgTabsTitle);
}

function selectImgTabsTitle() {
  for (let i = 0; i < imgTabsTitles.length; i++) {
    imgTabsTitles[i].classList.remove("amazing-work-tabs-title-active");
  }
  this.classList.add("amazing-work-tabs-title-active");
  imgTabsName = this.getAttribute("data-name");
  selectImgTabContent(imgTabsName);
  if (imgTabsName !== "all") {
    loadBtn.classList.add("hidden-img");
  } else {
    loadBtn.classList.remove("hidden-img");
  }
}

function selectImgTabContent(imgTabName) {
  for (let i = 0; i < imgTabsContent.length; i++) {
    if (imgTabsContent[i].classList.contains(imgTabName)) {
      imgTabsContent[i].classList.remove("hidden-img");
    } else {
      imgTabsContent[i].classList.add("hidden-img");
    }
  }
}

let loadBtn = document.querySelector(".loading-button");
let imgDiv = document.querySelector(".work-tabs-title-content");
let loadAnim = document.querySelector(".loading-animation");

loadBtn.addEventListener("click", (e) => {
  e.preventDefault();
  loadAnim.classList.remove("loading-animation");
  loadBtn.replaceWith(loadAnim);
  setTimeout(function () {
    imgDiv.classList.remove("all-overflow");
    loadAnim.remove();
  }, 2000);
});

new Glide(".glide", {
  type: "carousel",
}).mount();
